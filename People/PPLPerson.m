//
//  PPLPerson.m
//  People
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "PPLPerson.h"
#import "PPLDataFetcher.h"

@implementation PPLPerson

- (instancetype)initWithDictionary:(NSDictionary *)people
{
    NSLog(@"%@", people);
    self = [super init];
    self.name           = people[kPersonName];
    self.title          = people[kPersonTitle];
    self.age            = [self ageFromBirthDateString:(people[kPersonBirthDate])];
    self.imageURL       = people[kPersonImageURL];
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Name: %@, Title: %@, Age: %@", self.name, self.title, self.age];
}

- (void)loadImage
{
    PPLDataFetcher *dataFetcher = [[PPLDataFetcher alloc] init];
    
    
    [dataFetcher fetchImage:self.imageURL completionBlock:^(UIImage *image, NSError *error) {
        if (error) {
            NSLog(@"error: %@",[error localizedDescription]);
            self.image = [UIImage imageNamed:@"noImage"];
            [self.delegate updateImage];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.image = image;
                [self.delegate updateImage];
            });
        }
    }];
    
}

- (NSNumber *)ageFromBirthDateString:(NSString *)birthDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMMM dd, yyyy"];
    NSDate *birthday = [dateFormatter dateFromString: birthDate];
    
    NSDate* now = [NSDate date];
    NSDateComponents* ageComponents = [[NSCalendar currentCalendar]
                                       components:NSCalendarUnitYear
                                       fromDate:birthday
                                       toDate:now
                                       options:0];
    return @([ageComponents year]);
}

@end
