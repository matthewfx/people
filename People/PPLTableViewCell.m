//
//  PPLTableViewCell.m
//  People
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "PPLTableViewCell.h"

@interface PPLTableViewCell () <PersonDelegate>

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *personImageView;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation PPLTableViewCell

- (void)setPerson:(PPLPerson *)person
{
    _person = person;
    [self setupElements];
    
}

- (void)setupElements
{
    self.nameLabel.text = self.person.name;
    self.titleLabel.text = self.person.title;
    self.ageLabel.text = [self.person.age stringValue];
    
    if (self.person.imageURL && ![self.person.imageURL isEqualToString:@""]) {
        [self showImage];
    } else {
        self.personImageView.image = [UIImage imageNamed:@"noImage"];
    }
}

- (void)showImage
{
    if (self.person.image) {
        [self updateImage];
    } else {
        self.person.image = nil;
        self.spinner.hidden = NO;
        [self.spinner startAnimating];
        self.person.delegate = self;
        [self.person loadImage];
    }
}

- (void)updateImage
{
    [self.spinner stopAnimating];
    self.spinner.hidden = YES;
    
    if (self.person.image) {
        [self.personImageView setImage:self.person.image];
        self.personImageView.alpha = 0.0F;
        [UIView animateWithDuration:0.8F
                         animations:^{
                             self.personImageView.alpha = 1.0F;
                         }];
    } else {
        self.personImageView.image = [UIImage imageNamed:@"noImage"];
    }
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
