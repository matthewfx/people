//
//  PPLTableViewCell.h
//  People
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PPLPerson.h"

@interface PPLTableViewCell : UITableViewCell

@property (nonatomic, strong) PPLPerson *person;

@end
