//
//  PPLDataFetcher.m
//  People
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "PPLDataFetcher.h"
#import "PPLPerson.h"

NSString * const kPersonName                = @"name";
NSString * const kPersonTitle               = @"title";
NSString * const kPersonBirthDate           = @"date of birth";
NSString * const kPersonImageURL            = @"image";
NSString * const kPersonAge                 = @"age";

NSString * const kJSONURL                = @"https://dl.dropboxusercontent.com/s/8bz9e8aq223hcz7/test.json";

@interface PPLDataFetcher () <NSURLSessionDelegate>

@property (nonatomic, strong) NSURLSession *session;
@property (copy, nonatomic) PPLImageCompletionBlock block;

@end

@implementation PPLDataFetcher

- (id)init
{
    self = [super init];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    
    config.timeoutIntervalForRequest = 30.0;
    config.timeoutIntervalForResource = 30.0;
    
    _session = [NSURLSession sessionWithConfiguration:config delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    return self;
    
}

- (void)fetchPeopleWithCompletionBlock:(PPLPeopleCompletionBlock)block
{
    NSURL *url = [NSURL URLWithString: kJSONURL];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    
    NSURLSessionDataTask *downloadDataTask = [self.session
                                              dataTaskWithURL: url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                  
                                                  if (error) {
                                                      block(nil, error);
                                                  } else {
                                                      
                                                      NSError *jsonError;
                                                      NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data
                                                                                                             options:NSJSONReadingMutableContainers
                                                                                                               error:&jsonError];
                                                      
                                                      if (jsonError || result == nil) {
                                                          block(nil, jsonError);
                                                      } else {
                                                          
                                                          NSArray *array = result[@"people"];
                                                          NSMutableArray *people = [NSMutableArray new];
                                                          
                                                          for (NSDictionary *dictionary in array) {
                                                              PPLPerson *person = [[PPLPerson alloc] initWithDictionary:dictionary];
                                                              [people addObject:person];
                                                          }
                                                          block(people, nil);
                                                      }
                                                      
                                                  }
                                                  
                                                  dispatch_async(dispatch_get_main_queue(), ^{
                                                      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                                                  });
                                              }];
    [downloadDataTask resume];
    
}



- (void)fetchImage:(NSString *)imageURL
   completionBlock:(PPLImageCompletionBlock)block
{
    
    
    NSURL *url = [NSURL URLWithString:imageURL];
    
    
    self.block = block;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    });
    
    
    NSURLSessionDownloadTask *downloadTask = [self.session downloadTaskWithURL:url];
    [downloadTask resume];
}



- (void)URLSession:(NSURLSession *)session downloadTask:(NSURLSessionDownloadTask *)downloadTask didFinishDownloadingToURL:(NSURL *)location
{
    NSData *data = [NSData dataWithContentsOfURL:location];
    
    UIImage *image = [UIImage imageWithData:data];
    
    self.block(image, nil);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    });
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didCompleteWithError:(NSError *)error
{
    if (error) {
        NSURLRequest *request = task.originalRequest;
        NSURL *url = request.URL;
        
        NSLog(@"Error code %ld while downloading image from URL: %@", (long)error.code, [url absoluteString]);
        
        self.block (nil, error);
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        });
    }
}


@end
