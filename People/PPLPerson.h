//
//  PPLPerson.h
//  People
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol PersonDelegate <NSObject>

- (void)updateImage;

@end

@interface PPLPerson : NSObject

@property (nonatomic, weak) id <PersonDelegate> delegate;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, strong) UIImage  *image;
@property (nonatomic, strong) NSString *imageURL;


- (instancetype)initWithDictionary:(NSDictionary *)people;
- (void)loadImage;


@end
