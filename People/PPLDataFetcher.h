//
//  PPLDataFetcher.h
//  People
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>


extern NSString * const kPersonName;
extern NSString * const kPersonTitle;
extern NSString * const kPersonBirthDate;
extern NSString * const kPersonImageURL;
extern NSString * const kPersonAge;

typedef void(^PPLPeopleCompletionBlock)(NSArray *people, NSError *error);
typedef void(^PPLImageCompletionBlock)(UIImage *image, NSError *error);

@interface PPLDataFetcher : NSObject

- (void)fetchPeopleWithCompletionBlock:(PPLPeopleCompletionBlock)block;

- (void)fetchImage:(NSString *)imageURL
   completionBlock: (PPLImageCompletionBlock)block;


@end
