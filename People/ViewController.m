//
//  ViewController.m
//  People
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "ViewController.h"

#import "PPLPerson.h"
#import "PPLDataFetcher.h"
#import "PPLTableViewCell.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@property(nonatomic, strong) NSArray *people;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.spinner startAnimating];
    
    PPLDataFetcher *dataFetcher = [[PPLDataFetcher alloc] init];
    [dataFetcher fetchPeopleWithCompletionBlock:^(NSArray *people, NSError *error) {
        
        [self.spinner stopAnimating];
        self.spinner.hidden = YES;
        
        if (!error) {
            self.people = people;
            [self.tableView reloadData];
        } else {
            UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"Error!" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alerView show];
        }
        
    }];
}

#pragma mark TABLE VIEW

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.people count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"PersonCell";
    
    PPLTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.person = self.people[indexPath.row];
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
